#Exercices

Question une: liste des apprenants:


```
SELECT * FROM apprenant;
```



```
| id | nom               | adresse    | date_debut | date_fin   |
+----+-------------------+------------+------------+------------+
|  1 | Aigneis Abrahm    | Suite 68   | 2023-12-10 | 2024-06-13 |
|  2 | Tessie Devinn     | 1st Floor  | 2023-11-20 | 2024-06-13 |
|  3 | Paulie Dawtre     | Suite 41   | 2023-11-29 | 2024-06-13 |
|  4 | Audry Dobeson     | Apt 794    | 2023-11-16 | 2024-06-13 |
|  5 | Kirstyn Dawbery   | Room 713   | 2023-11-15 | 2024-06-13 |
|  6 | Yancy Tremoille   | Suite 27   | 2023-12-27 | 2024-06-13 |
|  7 | William Brothwood | Suite 50   | 2023-12-16 | 2024-06-13 |
|  8 | Evelyn Gimert     | Apt 1741   | 2023-11-13 | 2024-06-13 |
|  9 | Rosa Gianolini    | 16th Floor | 2023-12-24 | 2024-06-13 |
| 10 | Sebastien Ashness | Suite 6    | 2023-12-30 | 2024-06-13 |
| 11 | Carri Coghill     | Apt 1510   | 2023-12-03 | 2024-06-13 |
| 12 | Erminia Teece     | Suite 32   | 2023-12-19 | 2024-06-13 |
| 13 | Stanley Schriren  | 8th Floor  | 2023-12-25 | 2024-06-13 |
| 14 | Vanda Pandey      | Room 456   | 2023-12-11 | 2024-06-13 |
| 15 | Inez Nanninini    | Suite 9    | 2023-11-11 | 2024-06-13 |
| 16 | Dorthea Rayer     | Room 101   | 2023-12-01 | 2024-06-13 |
| 17 | Alexa Spraggon    | Room 1299  | 2023-11-30 | 2024-06-13 |
| 18 | Collete Denacamp  | PO Box 148 | 2023-11-08 | 2024-06-13 |
| 19 | Tynan Disbury     | Suite 86   | 2023-11-30 | 2024-06-13 |
| 20 | Rhodie Pretsel    | Room 559   | 2023-12-10 | 2024-06-13 |
+----+-------------------+------------+------------+------------+
```
