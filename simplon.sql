CREATE DATABASE IF NOT EXISTS simplon;
USE simplon;

CREATE TABLE IF NOT EXISTS apprenant (
  id int PRIMARY KEY,
  nom varchar(255),
  adresse varchar(255),
  date_debut date,
  date_fin date
);

CREATE TABLE IF NOT EXISTS jour (
  id_jour int PRIMARY KEY,
  date date
);

CREATE TABLE IF NOT EXISTS matos (
  id int PRIMARY KEY,
  type varchar(255),
  id_apprenant int
);

CREATE TABLE IF NOT EXISTS groupe (
  id varchar(50) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS presence (
  id int PRIMARY KEY,
  id_apprenant int,
  id_jour int,
  signature blob,
  id_groupe varchar(50),
  FOREIGN KEY (id_groupe) REFERENCES groupe(id),
  FOREIGN KEY (id_jour) REFERENCES jour(id_jour)
);

CREATE TABLE IF NOT EXISTS jourformation (
  id int PRIMARY KEY,
  date date,
  id_apprenant int,
  id_groupe varchar(50)
);

CREATE TABLE IF NOT EXISTS formation (
  id int PRIMARY KEY,
  nom varchar(255),
  date_fin date,
  date_debut date,
  id_jour int,
  id_apprenant int,
  id_groupe varchar(50),
  id_presence int,
  id_matos int,
  FOREIGN KEY (id_jour) REFERENCES jourformation(id) ON DELETE SET NULL,
  FOREIGN KEY (id_apprenant) REFERENCES apprenant(id),
  FOREIGN KEY (id_groupe) REFERENCES groupe(id),
  FOREIGN KEY (id_presence) REFERENCES presence(id),
  FOREIGN KEY (id_matos) REFERENCES matos(id)
);

-- Création de la table competences
CREATE TABLE IF NOT EXISTS competences (
  id int AUTO_INCREMENT PRIMARY KEY,
  nom varchar(255),
  id_apprenant int,
  id_groupe varchar(50),
  id_formation int,
  id_presence int,
  competence varchar(255)
);

-- Ajout de la contrainte FOREIGN KEY après la création de la colonne
ALTER TABLE competences
ADD FOREIGN KEY (id_apprenant) REFERENCES apprenant(id);


CREATE TABLE IF NOT EXISTS diplomes (
  id int PRIMARY KEY,
  nom varchar(255),
  id_apprenant int,
  id_groupe varchar(50),
  id_formation int,
  id_competences int,
  id_jour int,
  FOREIGN KEY (id_apprenant) REFERENCES apprenant(id),
  FOREIGN KEY (id_groupe) REFERENCES groupe(id),
  FOREIGN KEY (id_formation) REFERENCES formation(id),
  FOREIGN KEY (id_competences) REFERENCES competences(id),
  FOREIGN KEY (id_jour) REFERENCES jour(id_jour)
);

CREATE TABLE IF NOT EXISTS contrat (
  id int PRIMARY KEY,
  id_apprenant int,
  id_formation int,
  id_jour int,
  id_presence int,
  id_competences int,
  id_diplomes int,
  FOREIGN KEY (id_apprenant) REFERENCES apprenant(id),
  FOREIGN KEY (id_formation) REFERENCES formation(id),
  FOREIGN KEY (id_jour) REFERENCES jour(id_jour),
  FOREIGN KEY (id_presence) REFERENCES presence(id),
  FOREIGN KEY (id_competences) REFERENCES competences(id),
  FOREIGN KEY (id_diplomes) REFERENCES diplomes(id)
);
